function getApplicationCrmUrl(){
    return 'http://10.10.2.236/gamification/public/index.php/';
}
function getIframeData() {
    var str = window.location;
    var url = str.protocol+"//"+str.host + str.pathname;
    return url;
}

function getDefaultRoute(){
    return '/';
}

function msToTime(duration,withSeconds) {
var milliseconds = parseInt((duration%1000)/100)
    , seconds = parseInt((duration/1000)%60)
    , minutes = parseInt((duration/(1000*60))%60)
    , hours = parseInt((duration/(1000*60*60))%24);

hours = (hours < 10) ? "0" + hours : hours;
minutes = (minutes < 10) ? "0" + minutes : minutes;
seconds = (seconds < 10) ? "0" + seconds : seconds;
if(withSeconds){
    return hours + ":" + minutes + ":" + seconds;
}else{
    return hours + ":" + minutes;
}
}

function refreshTime(){
return 5000;
}
function refreshGamificationData(){
    return 5000;
}
function initCap(str) {
return str.charAt(0).toUpperCase() + str.slice(1);
}

