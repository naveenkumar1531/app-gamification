import {setCampaigns,setQueues,setSelectedCampaign,setUserScore,setSelectedQueue} from '../actions/gamificationActions'
var apiUrl = window.getApplicationCrmUrl();
var client = window.AmeyoClient.init();
export function getCampaignsList(){
        return dispatch=>{
            var requestObject = {
                url: "ameyorestapi/cc/campaigns/getAllCampaigns",
                headers: {
                    "content-type": "application/json"
                },
                method: "GET",
            };
            client.httpRequest.invokeAmeyo(requestObject).then(function(allCampaigns){
                dispatch(setCampaigns(JSON.parse(allCampaigns.response)));
            }).catch(function(error){
                console.log("all campaigns rae---",error)
            });
            // var res ='[{"campaignId": 38,"campaignName": "HDFC_Sales","campaignType": "Outbound Voice Campaign","description": ""},{"campaignId": 1,"campaignName": "HDFC_Support","campaignType": "Outbound Voice Campaign","description": ""}]';
           //  dispatch(setCampaigns(JSON.parse(res)));
             
         }
}

export function getAllQueueByCampaignId(campaignId) {
    return dispatch=>{
        dispatch(setSelectedCampaign(campaignId));
        let queueId=null;
        dispatch(setSelectedQueue(queueId));
        dispatch(getUserScoreByCampaignOrQueue());
        var requestObject = {
            url: "/ameyorestapi/cc/agentQueues/getByCampaign?campaignId="+campaignId,
            headers: {
                "content-type": "application/json"
            },
            method: "GET",
        };
        client.httpRequest.invokeAmeyo(requestObject).then(function(allQueues){
            dispatch(setQueues(JSON.parse(allQueues.response)));
        }).catch(function(error){
            console.log("all queues api error---",error)
        });    
        //var res ='[{"queueId": 38,"queueName": "HDFC_Sales_Queue"},{"queueId": 37,"queueName": "HDFC_Support_Queue"}]';
       // dispatch(setQueues(JSON.parse(res)));
        
    }
}
export function getUserScoreByCampaignOrQueue() {
    return (dispatch,getState)=>{
        var state=getState();
        let campaignId=(state.campaignReducer.selectedCampaign !== undefined)?state.campaignReducer.selectedCampaign:'';
        let queueId=(state.queueReducer.selectedQueue !== undefined)?state.queueReducer.selectedQueue:'';
        if(campaignId !== undefined && campaignId !==''){
            dispatch(getScoreInfo(campaignId,queueId));
        }
    }
}

export function getScoreInfo(campaignId,queueId) {
    return dispatch=>{
    let finalUrl =apiUrl + 'game/getScoreInfo/'+campaignId;
    fetch(finalUrl, {
    method: 'Get',
    }).then((response)=>{
        return response.json();
    }).then((data)=>{
       dispatch(getUserScore(data,queueId));
    }).catch((err)=>{
       
    });
  }
}
export function getUserScore(scoreInfo,queueId) {
    return (dispatch,getState)=>{
    try{
        let state=getState();
        let userId=state.agentMonitoringData.agentLoginData.userId;
        let userType=state.agentMonitoringData.agentLoginData.userType;
        let totalScore = scoreInfo.length;
        let scoreIds=[];
       // scoreInfo.forEach(element => {
       //     scoreIds.push(element.score_id);
       // });
       scoreInfo.forEach((element) => {   
            if(queueId !== null){
                if(element.context ==='Queue' && parseInt(element.context_id)===parseInt(queueId)){
                   
                    scoreIds.push(element.score_id);
                }
            } else {
                scoreIds.push(element.score_id);    
            }
            
        });
        let finalUrl =apiUrl + `game/getScorebyUser/${userId}/${userType}/${JSON.stringify(scoreIds)}`;
        fetch(finalUrl, {
        method: 'Get',
        }).then((response)=>{
            return response.json();
        }).then((data)=>{
           dispatch(setUserScore(data));
        }).catch((err)=>{
            console.log('Unable to fetch Score');
        });
    } catch(e) {
         console.log('No Score founds',e);
    }
   
   }
}
