import {setAgentData, setUserLoginData, 
        setAgentCampaignData, setAgentSelectionData, setAgentLoginData, setAgentBreakData} 
from '../actions/agentMonitoringAction'

export function dataCombine(){
    return (dispatch, getState)=>{
        var state = getState();
        var finalData = {};
        var userId = state.agentMonitoringData.agentLoginData.userId;
        var campaignData = state.agentMonitoringData.agentCampaignData;
        finalData[userId] = {"userId":userId,"campaignData":campaignData};
       // console.log("finalData",finalData)
        dispatch(setAgentSelectionData(finalData))        
    } 
}

export function getUserLoginData() {    
    var client = window.AmeyoClient.init();
    return (dispatch, getState)=>{
        var state = getState();
        var loggedInData = [];
        var selectedCampaignsData = "";
        var finalData = ""
        client.globalData.get("loggedInUser").then(function(responseLoginData) {        
            dispatch(setUserLoginData(responseLoginData))    
            client.globalData.get("selectedCampaigns").then(function(responseCampaignData) {            
                dispatch(setAgentCampaignData(responseCampaignData))                
                dispatch(dataCombine())                
                dispatch(getAgentCallData())
                dispatch(getAgentBreakStats())                
            }).catch(function(error) {
                console.log("selectedCampaigns errors is",error);
            });
        }).catch(function(error) {
            console.log("loggedInUser error is",error);
    });
  }
}

export function getAgentBreakStats() {
    var client = window.AmeyoClient.init();
    var requestBody = JSON.stringify({
        "statsName": "AgentBreakStats"        
    });
    var requestObject = {
        url: "ameyorestapi/stats/getAllStats",
        headers: {
            "content-type": "application/json"
        },
        method: "POST",
        body: requestBody
    };
    return (dispatch, getState) => {        
        try{
            client.httpRequest.invokeAmeyo(requestObject).then(function(responseBreakData) {
                if(responseBreakData.statusCode === 200){
                    dispatch(setAgentBreakData(JSON.parse(responseBreakData.response)))
                }                
            });
        }catch(e){            
            console.log("error is",e)
        }
    }
}

export function getAgentCallData() {
    var client = window.AmeyoClient.init();
    var requestBody = JSON.stringify({
        "statsName": "AgentCallStatsT2D2"        
    });
    var requestObject = {
        url: "ameyorestapi/stats/getAllStats",
        headers: {
            "content-type": "application/json"
        },
        method: "POST",
        body: requestBody
    };
    return (dispatch, getState) => {        
        try{
            client.httpRequest.invokeAmeyo(requestObject).then(function(responseCallData) {
              //  console.log("responseCallData>>>",responseCallData)
                if(responseCallData.statusCode === 200){
                    dispatch(setAgentData(JSON.parse(responseCallData.response)))
                }                
            });
        }catch(e){            
            console.log("error is",e)
        }
    }
}
