const camapaignReducer = (state={},action) => {
    switch(action.type){
        case 'GET_ALL_CAMPAIGNS':
        return {
            ...state,
             allCampaigns: action.data
        }
        case 'CURRENT_SELECTED_CAMPAIGN':
        return {
            ...state,
             selectedCampaign: action.campaignId
        }
         default:
          return state;
     }

    }
export default camapaignReducer;
