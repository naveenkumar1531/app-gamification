const agentMonitoringData=(state={},action) =>{
switch(action.type){
	case 'SET_AGENT_DATA':
	var totalTalkTime = 0;
	var totalWrapTime = 0;
	var totalCalls = 0;
	var totalHoldTime = 0;
	var totalHoldCount = 0;
	var agentCallData = {}	
	try{
		var data = action.agentData[0].statsArr
	    Object.keys(data).forEach((key)=>{
		Object.keys(data[key].statsArr).forEach((nextKey)=>{
			if(state.agentSelectionData[data[key].statsArr[nextKey].user_id] !== undefined){
				if(data[key].statsArr[nextKey].user_id === state.agentSelectionData[data[key].statsArr[nextKey].user_id].userId){
					//Object.keys(state.agentSelectionData[data[key].statsArr[nextKey].user_id].campaignData).forEach((campKey)=>{
						//if(data[key].statsArr[nextKey].campaign_id === state.agentSelectionData[data[key].statsArr[nextKey].user_id].campaignData[campKey].campaignId){
							totalTalkTime += data[key].statsArr[nextKey].userCampaignTalkTimeTotal
							totalWrapTime += data[key].statsArr[nextKey].userCampaignWrapTimeTotal
							totalCalls += data[key].statsArr[nextKey].userCampaignCallConnectedCount
							totalHoldTime += data[key].statsArr[nextKey].holdTimeTotal
							totalHoldCount += data[key].statsArr[nextKey].holdCallsCount		
						//}
						
					//})
				}
			}
		})
	})
	} catch(e){

	}
	
	agentCallData['avgHandlingTime'] = window.msToTime(Math.round((totalTalkTime + totalWrapTime)/(totalCalls === 0 ? 1 : totalCalls)),true)
	agentCallData['avgTalkTime'] = window.msToTime(Math.round(totalTalkTime/(totalCalls === 0 ? 1 : totalCalls)),true)
	agentCallData['avgWrapTime'] = window.msToTime(Math.round(totalWrapTime/(totalCalls === 0 ? 1 : totalCalls)),true)
	agentCallData['avgHoldTime'] = window.msToTime(Math.round(totalHoldTime/(totalHoldCount === 0 ? 1 : totalHoldCount)),true)
	agentCallData['totalCalls'] = totalCalls
	
	return {
		...state,
		callData : agentCallData
	}

	case 'SET_AGENT_LOGIN_DATA' :
	return {
		...state,
		agentLoginData : action.agentLoginData
	}

	case 'SET_AGENT_CAMPAIGN_DATA' : 
	return {
		...state,
		agentCampaignData : action.agentCampaignData
	}

	case 'SET_AGENT_COMBINE_DATA':
	return {
		...state,
		agentSelectionData : action.data
	}

	// case 'SET_AGENT_LOGIN_STATS' : 
	// var loginData = action.loginStats[0].statsArr;	
	// var userLoginDuration = 0;		
	// Object.keys(loginData).forEach((key)=>{
	// 	Object.keys(loginData[key].statsArr).forEach((nextKey)=>{
	// 		if(state.agentSelectionData[loginData[key].statsArr[nextKey].user_id] !== undefined){
	// 			if(loginData[key].statsArr[nextKey].user_id === state.agentSelectionData[loginData[key].statsArr[nextKey].user_id].userId){
	// 				Object.keys(state.agentSelectionData[loginData[key].statsArr[nextKey].user_id].campaignData).forEach((campKey)=>{
	// 					if(loginData[key].statsArr[nextKey].campaign_id === state.agentSelectionData[loginData[key].statsArr[nextKey].user_id].campaignData[campKey].campaignId){
	// 						userLoginDuration = loginData[key].statsArr[nextKey].userCampaignLoginDurationTotal
	// 					}	
	// 				})					
	// 			}
	// 		}
	// 	})				
	// })
	// console.log("userLoginDuration>>",userLoginDuration)
	// return {
	// 	...state,
	// 	agentLoginStats : window.msToTime(userLoginDuration)
	// }

	case 'AGENT_BREAK_STATS' :
	try{
	var breakData = action.breakStats[0].statsArr[0];		
	var userBreakData = {};
	var userLoginDuration = 0;
	var totalBreakDuration = 0;
	Object.keys(breakData).forEach((key)=>{				
		Object.keys(state.agentSelectionData).forEach((agentKey)=>{
			if(breakData[key].user_id === state.agentSelectionData[agentKey].userId){
				if(breakData[key].agent_break_reason !== null && breakData[key].agent_break_reason !== 'agent_selected_campaign'){
					var breakReason = window.initCap(breakData[key].agent_break_reason)
					userBreakData[breakReason] = window.msToTime((Math.round(breakData[key].break_duration) * 1000),true)
				}
				userLoginDuration = breakData[key].login_duration
				totalBreakDuration += breakData[key].break_duration
				
			}
		})		
	})	
	userLoginDuration = Math.round(userLoginDuration) * 1000
	totalBreakDuration = Math.round(totalBreakDuration) * 1000
	//console.log("userBreakData>>>",userBreakData)
	 return{
		...state,
		agentBreakStats : userBreakData,
		agentLoginStats : window.msToTime(userLoginDuration),
		totalBreakDuration : window.msToTime(totalBreakDuration,true)
	 }
	} catch(e) {
		return state;
	}
	

	default:
		return state;
  }
}
export default agentMonitoringData