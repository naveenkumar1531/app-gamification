const uiSelection=(state={},action) =>{
switch(action.type){
	case 'SET_EXTERNAL_STATE':
	
	var copy = Object.assign({},state);
	if(copy.externalStates ===undefined){
		var externalStates=action.externalState;
		return {
                ...state,
                externalStates:externalStates
        }
	} 
	else {
		var copy2 = Object.assign({},copy.externalStates)
		copy2.externalStates =action.externalState;
		return {
                ...state,
                externalStates:copy2.externalStates
        }
	}

	case 'SET_LOADER':

	var copy = Object.assign({},state);
	if(copy.loader ===undefined){
		var loaderStatus=action.status;
		return {
                ...state,
                loader:loaderStatus
        }
	} 
	else {
		var copy2 = Object.assign({},copy.loader)
		copy2.loader =action.status;
		return {
                ...state,
				loader:copy2.loader
		}
	}
	
	default:
		return state;
  }
}
export default uiSelection