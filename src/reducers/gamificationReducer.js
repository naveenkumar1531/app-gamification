import { combineReducers } from 'redux';
import campaignReducer from './campaignReducer'
import queueReducer from './queueReducer'
import userScoreReducer from './userScoreReducer'
import agentMonitoringData from './AgentMonitoringReducer'
const rootCallReducer = combineReducers({
    campaignReducer,
    queueReducer,
    userScoreReducer,
    agentMonitoringData
})

export default rootCallReducer
