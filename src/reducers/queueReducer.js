const queueReducer = (state={},action) => {
    switch(action.type){
        case 'GET_ALL_QUEUES':
        return {
            ...state,
             allQueues: action.data
        }
        case 'CURRENT_SELECTED_QUEUE':
        return {
            ...state,
             selectedQueue: action.queueId
        }
         default:
          return state;
     }

    }
export default queueReducer;
