import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import rootCallReducer from '../reducers/gamificationReducer'


const configureStore = preloadedState => {
  const store = createStore(
    rootCallReducer,
    preloadedState,
    compose(
      applyMiddleware(thunk,  logger)
      
    )
  )

 
  return store
}

export default configureStore
