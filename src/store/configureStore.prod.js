import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

import rootCallReducer from '../reducers/gamificationReducer'


const configureStore = preloadedState => {
  const store = createStore(
    rootCallReducer,
    preloadedState,
    compose(
      applyMiddleware(thunk)
      
    )
  )

 
  return store
}

export default configureStore
