import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'

import {
  HashRouter,
  Route
} from 'react-router-dom';

import App from './App'
//import AgentMonitoring from '../components/agentSignatureComponent/AgentMonitoring';
import ModalButton from './components/agentSignatureComponent/ModalButton';
import Gamification from './Gamification';

const defaultRoute =  window.getDefaultRoute();
const Root = ({ store}) => (

  <Provider store={store}>
  <HashRouter>
    <div>
    <Route  exact path={defaultRoute} component={ModalButton}/>
    <Route path="/gamification" component={Gamification}/>    
   </div>
   </HashRouter>  
 </Provider>
 
)


Root.propTypes = {
  store: PropTypes.object.isRequired
 
}

export default Root
