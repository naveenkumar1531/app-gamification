import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Modal,Button} from 'react-materialize';
import Filter from  './components/Filter'
import AgentSignature from './components/agentSignatureComponent/AgentSignature'
import UserScore from  './components/UserScoreStats'

class Gamification extends Component {
  render() {
    return (
        /*
      <div className="App">

          <Modal className="modal-large modal-fixed-footer hide-footer gamification-modal"
                         actions={<div className="multiple-btn "> <Button  className="btn btn-primary">Update</Button> <Button modal="close" className="btn btn-light">Cancel</Button></div>}
                        header={<div className="modal-header clearfix"><h4> Let the games begin!</h4><a className="modal-close"> <i className="zmdi zmdi-close"></i></a></div>}
                         trigger={<Button  className="btn btn-light default-btn">Set Refersh Time</Button>}>
                        <div className="modal-inner-content">
                        <div className="main">
          <div className="content">
           <Filter/>
            <div className="row user-performance-wrapper">
            <UserScore/>
            </div>
          <AgentSignature/>
          </div>
        </div>
      </div>
                         
     </Modal>

    </div>*/
    <div className="App">
    <div className="modal-inner-content">
        <div className="main">
          <div className="content">
           <Filter/>
            <div className="row user-performance-wrapper">
            <UserScore/>
            </div>
          <AgentSignature/>
          </div>
        </div>
      </div>
   </div>               

    );
  }
}

export default Gamification;