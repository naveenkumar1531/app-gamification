export const setCampaigns=(data)=>{
    return{
      type:'GET_ALL_CAMPAIGNS',
      data
    }
  }
export const setQueues=(data)=>{
    return{
        type:'GET_ALL_QUEUES',
        data
      }
}
export const setSelectedCampaign=(campaignId)=>{
    return{
        type:'CURRENT_SELECTED_CAMPAIGN',
        campaignId
    }
}
export const setUserScore=(data)=>{
    return{
        type:'SET_ALL_USER_SCORE',
        data
    }
}
export const setSelectedQueue=(queueId)=>{
    return{
        type:'CURRENT_SELECTED_QUEUE',
        queueId
    }
}
