export const  setAgentData=(agentData)=>{
    return{
        type:'SET_AGENT_DATA',
        agentData
    }
}
export const setUserLoginData = (agentLoginData) =>{        
    return {
        type : 'SET_AGENT_LOGIN_DATA',
        agentLoginData
    }
}

export const setAgentCampaignData = (agentCampaignData) => {
    return {
        type : 'SET_AGENT_CAMPAIGN_DATA',
        agentCampaignData
    }
}

export const setAgentSelectionData = (data) => {
    return {
        type : 'SET_AGENT_COMBINE_DATA',
        data
    }
}
export const setAgentLoginData = (loginStats) => {
    return {
        type : 'SET_AGENT_LOGIN_STATS',
        loginStats
    }
}
export const setAgentBreakData= (breakStats) =>{
    return{
        type : 'AGENT_BREAK_STATS',
        breakStats
    }
}