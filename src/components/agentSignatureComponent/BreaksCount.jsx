import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import {getSelectedTicket} from '../../middlewares/agentMonitoringMiddlware'


const BreakTypes=({breakTypesData})=>{  
  var rows = [];
  var breakTypes = breakTypesData === undefined ? '' : breakTypesData;
  Object.keys(breakTypes).forEach((key)=>{
    var data = breakTypes[key].split(":")
    rows.push(
        <li key={key}>
            <div className="monitoring-data">
                <div className="value">
                  <strong>{data[0]}h</strong>
                  <span className="sec">{data[1]}m</span>
                  <span className="sec">{data[2]}s</span>
                </div>
                <span className="label">{key === '' ? 'Others' : key}</span>
            </div>
        </li>
      )
  })    
  return (
    <div className="row">
      <div className="col 12 m12 l12">
        <ul className="live-data user-summary clearfix">
          {rows}
        </ul>
      </div>
    </div>
  )      
}

class BreaksCount extends Component {
  render() {
    var totalBreakDuration = this.props.agentTotalBreakStats === undefined ? '' : this.props.agentTotalBreakStats.split(":");        
        return (
          <div className="row">
            <div className="col s12 m12 l12">
              <div className="total-break-duration clearfix">
               <div className="displayInline">
                <span className="label left">Total Break Duration: </span>

                  <div className="value left">
                    <strong>{totalBreakDuration[0]}h 
                    </strong>
                    <span className="sec">{totalBreakDuration[1]}m</span>
                    <span className="sec">{totalBreakDuration[2]}s</span>
                  </div>
                </div>
                </div>
            </div>
              <BreakTypes breakTypesData = {this.props.agentBreakStats}/>
          </div>
        )
  	}
}

const mapStateToProps = (state, ownProps) => ({
  agentBreakStats: state.agentMonitoringData === undefined ? '' : state.agentMonitoringData.agentBreakStats,
  agentTotalBreakStats: state.agentMonitoringData === undefined ? '' : state.agentMonitoringData.totalBreakDuration
})
const mapDispatchToProps = (dispatch, ownProps) => ({

})

export default connect(mapStateToProps,mapDispatchToProps)(BreaksCount)


