import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import {getSelectedTicket} from '../../middlewares/agentMonitoringMiddlware'

class CallsHandled extends Component {
  render() {
    var avgHandlingTime = this.props.agentCallStats === undefined ? '' : this.props.agentCallStats.avgHandlingTime.split(":")
    var avgTalkTime = this.props.agentCallStats === undefined ? '' : this.props.agentCallStats.avgTalkTime.split(":")
    var avgWrapTime = this.props.agentCallStats === undefined ? '' : this.props.agentCallStats.avgWrapTime.split(":")
    var avgHoldTime = this.props.agentCallStats === undefined ? '' : this.props.agentCallStats.avgHoldTime.split(":")
         return (
          <div className="row">
            <div className="col s12 m12 l12">
                <ul className="live-data user-summary call-handled clearfix">

                    <li>
                        <div className="monitoring-data">
                            <div className="value"><strong>{avgHandlingTime[1]}m </strong><span className="sec">{avgHandlingTime[2]}s</span></div>
                            <span className="label">Average Handling Time </span>
                        </div>
                    </li>
                    <li>
                        <div className="monitoring-data">
                            <div className="value"><strong>{avgTalkTime[1]}m </strong><span className="sec">{avgTalkTime[2]}s</span></div>
                            <span className="label">Average Talk Time</span>
                        </div>
                    </li>
                    <li>
                        <div className="monitoring-data">
                            <div className="value"><strong>{avgWrapTime[1]}m </strong><span className="sec">{avgWrapTime[2]}s</span></div>
                            <span className="label">Average Wrap Time</span>
                        </div>
                    </li>
                    <li>
                        <div className="monitoring-data">
                            <div className="value"><strong>{avgHoldTime[1]}m </strong><span className="sec">{avgHoldTime[2]}s</span></div>
                            <span className="label">Average Hold Time</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>      
      )
  	}
}

const mapStateToProps = (state, ownProps) => ({
    agentCallStats : state.agentMonitoringData === undefined ? '' : state.agentMonitoringData.callData
})
const mapDispatchToProps = (dispatch, ownProps) => ({

})

export default connect(mapStateToProps,mapDispatchToProps)(CallsHandled)


