import React, { Component } from 'react';
import { connect } from 'react-redux'
import {getSelectedTicket} from '../../middlewares/agentMonitoringMiddlware'
import CallsHandled from './CallsHandled'
import BreaksCount from './BreaksCount'
import {getUserLoginData} from '../../middlewares/agentMonitoringMiddlware'
import Loader from './Loader'
class AgentSignature extends Component {
  constructor(props){
        super(props);
  }
  componentDidMount(){        
    this.timer =setInterval(this.props.agentCallData, window.refreshTime());    
  }
  componentWillUnmount(){
    clearInterval(this.timer);    
  }
  render(){
    var loginDuration = this.props.agentLoginStats === undefined ? '' : this.props.agentLoginStats.split(":");    
    var totalCalls = this.props.agentCallStats === undefined ? '' : this.props.agentCallStats.totalCalls;
    var breakCount = Object.keys(this.props.agentBreakStats === undefined ? '' : this.props.agentBreakStats).length
   // console.log("this.props.agentMonitoringData",this.props.agentMonitoringData)
    if(Object.keys(this.props.agentMonitoringData).length !== 0){   
      return(
        <div className="row user-stats-wrapper">
        <div className="col s12 m12 l12">
          <div className="row">
              <div className="col s12 m12 l12 center-align">
                <h2 className="user-stats-wrapper_heading">your stats</h2>
              </div>
            </div>
            <div className="row">
              <div className="col s12 m12 l12">
              <div className="row top-heading mt10 center-align">
     <div className="col s12 m12 l12">
       <div className="displayInline">
        <i className="zmdi zmdi-timer  left"></i>
        <h4 className="left">Login Duration:</h4>
         <strong className="left ml10">{loginDuration[0]}h</strong><span className="left">{loginDuration[1]}m</span>
         <span className="left last-updated">(Last Updated: 5 min ago)</span>
         </div>
     </div>
    
  </div>
  <div className="row heading-highlighter mt10 mb10">
     <div className="col s12 m12 l12">
        <h4>Calls Handled:</h4>
        <h3>{totalCalls}</h3>
     </div>
  </div>
  <CallsHandled/>
   <div className="row heading-highlighter mt10 mb10">
     <div className="col s12 m12 l12">
        <h4>Breaks Count:</h4>
        <h3>{breakCount}</h3>
     </div>
  </div>
    <BreaksCount/>
              </div>
            </div>
        </div>
       
      </div>

      )
    }else{
        return(
          <div>
            <Loader/>
          </div>
          )
      }
    }
}
const mapStateToProps  = (state, ownProps) => ({
    agentMonitoringData: state.agentMonitoringData === undefined ? '' :  state.agentMonitoringData,
    agentLoginStats: state.agentMonitoringData === undefined ? '' : state.agentMonitoringData.agentLoginStats,
    agentCallStats : state.agentMonitoringData === undefined ? '' : state.agentMonitoringData.callData,
    agentBreakStats: state.agentMonitoringData === undefined ? '' : state.agentMonitoringData.agentBreakStats,
  })
  
  const mapDispatchToProps = (dispatch, ownProps) => {  
    var refreshAgentData = () => {
        dispatch(getUserLoginData())
    }  
    return {
      agentCallData: refreshAgentData
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(AgentSignature)
