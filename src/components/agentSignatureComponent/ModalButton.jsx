import React, { Component } from 'react'
import logo from '../../index.png';

const openModal = () => {
  var client = window.AmeyoClient.init();
  var iframeUrl= window.getIframeData();
  var modalCreationData = {    
    title: "Let the games begin!",
    url: iframeUrl+"#/gamification",
    height: 85,
    width: 75
  };
  var contextData = {
    "k1": "v1",
    "k2": "v2"
  };

  client.instance.create("modal", modalCreationData, contextData).then(successCallback).catch(failureCallback);  
 }

function successCallback(){
  //console.log('success');
}
function failureCallback(failure){
   console.log('failure'+JSON.stringify(failure));
}

class ModalButton extends Component {
  constructor(props) {
    super(props) 
  }
  shouldComponentUpdate(){
    return true
  }
  render() {
    return (
      <div>
        <button type="button" className="btn agentButton" onClick={()=>openModal()}>
          <img src={logo} />
        </button>
      </div>
    )
  }
}

export default ModalButton


