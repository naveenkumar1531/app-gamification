import React from 'react'
import { Link } from 'react-router'
import Loader from './Loader'
import { connect } from 'react-redux'
const FooterPanel = ({loader}) => {
   return(
   		  <div>
           <div className="bulk-action__footer">
                <Link to="/main"><button id="actionClose" type="button" className="btn btn-light">Cancel</button></Link>
                <button type="submit" className="btn btn-primary">Apply</button>
           </div>
           <Loader isLoaderEnabled={loader}/>
           </div>

      );
        };
 const mapStateToProps = (state, ownProps) => {
  return {
      loader:(state.uiSelection !== undefined) ? (state.uiSelection.loader !==undefined ? state.uiSelection.loader : undefined) : undefined
   }
}
const mapDispatchToProps = (dispatch, ownProps) => {
   
  return {

   
    
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(FooterPanel)

