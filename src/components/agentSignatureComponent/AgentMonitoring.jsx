import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import {getSelectedTicket} from '../middlewares/agentMonitoringMiddlware'
import CallsHandled from './CallsHandled'
import BreaksCount from './BreaksCount'
import {getUserLoginData} from '../middlewares/agentMonitoringMiddlware'
import Loader from './Loader'

class AgentMonitoring extends Component {
  constructor(props) {
    super(props) 
  }

  componentDidMount(){        
    this.timer =setInterval(this.props.agentCallData, window.refreshTime());    
  }
  componentWillUnmount(){
    clearInterval(this.timer);    
  }
  render() {
    var loginDuration = this.props.agentLoginStats === undefined ? '' : this.props.agentLoginStats.split(":");    
    var totalCalls = this.props.agentCallStats === undefined ? '' : this.props.agentCallStats.totalCalls;
    var breakCount = Object.keys(this.props.agentBreakStats === undefined ? '' : this.props.agentBreakStats).length
    console.log("this.props.agentMonitoringData",this.props.agentMonitoringData)
    if(Object.keys(this.props.agentMonitoringData).length !== 0){    
        return (
          <div>
            <div className="row top-heading mt10">
              <div className="col s6 m6 l6">
                <i className="mdi mdi-timer left" data-name="mdi-timer"></i>
                <h4 className="left">Login Duration:</h4>
                <strong className="left ml5">{loginDuration[0]}h</strong> <span className="left">{loginDuration[1]}m</span>
              </div>
            <div className="col s6 m6 l6 right-align">
              <span className="last-updated">(Last Updated: 5 min ago)</span>
            </div>
            </div>    
            <div className="row heading-highlighter mt10 mb10">
              <div className="col s12 m12 l12">
                <h4 className="left">Calls Handled:</h4>
                <h3 className="left">{totalCalls}</h3>
              </div>
            </div>
            <CallsHandled/>
            <div className="row heading-highlighter mt10 mb10">
              <div className="col s12 m12 l12">
                <h4 className="left">Breaks Count:</h4>
                <h3 className="left">{breakCount}</h3>
              </div>
            </div>
            <BreaksCount/>
          </div>           
        )
      }else{
        return(
          <div>
            <Loader/>
          </div>
          )
      }
    }
}

const mapStateToProps  = (state, ownProps) => ({
  agentMonitoringData: state.agentMonitoringData === undefined ? '' :  state.agentMonitoringData,
  agentLoginStats: state.agentMonitoringData === undefined ? '' : state.agentMonitoringData.agentLoginStats,
  agentCallStats : state.agentMonitoringData === undefined ? '' : state.agentMonitoringData.callData,
  agentBreakStats: state.agentMonitoringData === undefined ? '' : state.agentMonitoringData.agentBreakStats,
})

const mapDispatchToProps = (dispatch, ownProps) => {  
  var refreshAgentData = () => {
      dispatch(getUserLoginData())
  }  
  return {
    agentCallData: refreshAgentData
  }
}


export default connect(mapStateToProps,mapDispatchToProps)(AgentMonitoring)


