import React, { Component } from 'react';
import { connect } from 'react-redux'
import {getSelectedTicket} from '../middlewares/agentMonitoringMiddlware'
import CallsHandled from './CallsHandled'
import BreaksCount from './BreaksCount'
import {getUserLoginData} from '../middlewares/agentMonitoringMiddlware'
import Loader from './Loader'
class AgentSignature extends Component {
  constructor(props){
        super(props);
  }
  render(){
      return(
        <div className="row user-stats-wrapper">
        <div className="col s12 m12 l12">
          <div className="row">
              <div className="col s12 m12 l12 center-align">
                <h2 className="user-stats-wrapper_heading">your stats</h2>
              </div>
            </div>
            <div className="row">
              <div className="col s12 m12 l12">
              <div className="row top-heading mt10">
     <div className="col s6 m6 l6">
        <i className="zmdi zmdi-timer  left"></i>
        <h4 className="left">Login Duration:</h4>
         <strong className="left ml10">7h</strong> <span className="left">5m</span>
     </div>
     <div className="col s6 m6 l6 right-align">
        <span className="last-updated">(Last Updated: 5 min ago)</span>
     </div>
  </div>
  <div className="row heading-highlighter mt10 mb10">
     <div className="col s12 m12 l12">
        <h4>Calls Handled:</h4>
        <h3>68</h3>
     </div>
  </div>
  <div className="row">
     <div className="col s12 m12 l12">
        <ul className="live-data user-summary call-handled clearfix">
       
           <li>
              <div className="monitoring-data">
                 <div className="value"><strong>6m </strong><span className="sec"></span></div>
                 <span className="label">Average Handling Time </span>
              </div>
           </li>
           <li>
              <div className="monitoring-data">
                 <div className="value"><strong>4m </strong><span className="sec"></span></div>
                 <span className="label">Average Talk Time</span>
              </div>
           </li>
           <li>
              <div className="monitoring-data">
                 <div className="value"><strong>1m </strong><span className="sec">15s</span></div>
                 <span className="label">Average Wrap Time</span>
              </div>
           </li>
           <li>
              <div className="monitoring-data">
                 <div className="value"><strong>1m </strong><span className="sec">12s</span></div>
                 <span className="label">Average Hold Time</span>
              </div>
           </li>
        </ul>
     </div>
  </div>
  
   <div className="row heading-highlighter mt10 mb10">
     <div className="col s12 m12 l12">
        <h4>Breaks Count:</h4>
        <h3>5</h3>
     </div>
  </div>
  
  
  
  
  <div className="row">
   <div className="col s12 m12 l12">
              <div className="total-break-count clearfix">
                 <span className="label left">Total Break Duration: </span>
                 <div className="value left"><strong>1h </strong><span className="sec">20m</span></div>
                
              </div>
   </div>
     <div className="row">
     <div className="col 12 m12 l12">
        <ul className="live-data user-summary clearfix">
           <li>
              <div className="monitoring-data">
                 <div className="value"><strong>10m </strong><span className="sec">10s</span></div>
                 <span className="label">Training</span>
              </div>
           </li>
           <li>
              <div className="monitoring-data">
                 <div className="value"><strong>30m </strong><span className="sec">10s</span></div>
                 <span className="label">Lunch </span>
              </div>
           </li>
           <li>
              <div className="monitoring-data">
                 <div className="value"><strong>20m </strong><span className="sec">0s</span></div>
                 <span className="label">Prayer </span>
              </div>
           </li>
           <li>
              <div className="monitoring-data">
                 <div className="value"><strong>15m </strong><span className="sec">10s</span></div>
                 <span className="label">Snacks</span>
              </div>
           </li>
           <li>
              <div className="monitoring-data">
                 <div className="value"><strong>5m </strong><span className="sec">10s</span></div>
                 <span className="label">Anything</span>
              </div>
           </li>
        
        </ul>
     </div>
      </div>
  </div>
              </div>
            </div>
        </div>
       
      </div>

      )
    }
}

export default AgentSignature;