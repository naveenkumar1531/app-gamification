import React, { Component } from 'react';
import { connect } from 'react-redux'
import {Input} from 'react-materialize';
import {getAllQueueByCampaignId} from '../middlewares/gamificationMiddleware'
import {setSelectedQueue} from '../actions/gamificationActions'
class Filter extends Component {
  constructor(props){
        super(props);
  }
  render(){
      const {campaignList,queuesList,onCampaignSelection,onQueueSelection}=this.props;
      let campaignOptions=[];
      campaignOptions.push(<option key="0" value="">Choose an option</option>);
      if(campaignList && campaignList !=='') {
          try{
            campaignList.forEach(item => {
                campaignOptions.push(<option value={item.campaignId} key={item.campaignId}>{item.campaignName}</option>);       
            });
          } catch(e){

          }
          
       
      }
      let queueOptions=[];
      queueOptions.push(<option key="0" value="">Choose an option</option>);
      if(queuesList && queuesList !=='') {
          try{
            queuesList.forEach(item => {
                queueOptions.push(<option value={item.agentQueueId} key={item.queueId}>{item.queueName}</option>);       
          });
          }catch(e){

          }
            
        
      }
     return(
        <div className="assignment-wrapper row">
        <div className="col s12 m12 l12 campaign-queue-wrapper">
        <div className="campaign-queue-listing">
        <div className="campaign-listing">
        <div className="input-field mt0 clearfix">
            <Input s={12} m={12} l={12} type='select' id="campaign" onChange={(e)=>{onCampaignSelection(e.target.value)}}>
                {campaignOptions}
            </Input>
            <label>Campaign</label>
        </div>
        </div>
        <div className="arrow-icon">
          <i className="zmdi zmdi-chevron-right"></i>
        </div>
        <div className="queue-listing">
        <div className="input-field mt0 clearfix">
            <Input s={12} m={12} l={12} type='select' id="queue" onChange={(e)=>{onQueueSelection(e.target.value)}}>
                {queueOptions}
            </Input>
          <label>Queue</label>
        </div>
             </div>
             </div>
             </div>
      </div>

      )
  }

}
const mapStateToProps = (state, ownProps) => {
    return {
        campaignList:(state.campaignReducer.allCampaigns === undefined)?'':state.campaignReducer.allCampaigns,
        queuesList:(state.queueReducer.allQueues === undefined)?'':state.queueReducer.allQueues

   }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    let getQueues = (value)=>{
        dispatch(getAllQueueByCampaignId(value))
    }
    let selectedQueues=(queueId)=>{
        dispatch(setSelectedQueue(queueId));
    }
   return {
    onCampaignSelection:getQueues,
    onQueueSelection:selectedQueues

    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Filter)
