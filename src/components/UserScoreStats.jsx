import React, { Component } from 'react';
import { connect } from 'react-redux'
import RenderGraph from './RenderGraph'
import {getUserScoreByCampaignOrQueue} from '../middlewares/gamificationMiddleware'
const RenderTop5=(props)=>{
      let liList=[];
      props.topFiveUser.forEach(element=>{
          liList.push(<li key={element.userId}><span className="top_list_rank">{element.rank}</span><span className="top_list_name">{element.userId}</span></li>);
      })
    return(
        <div className="rank-section clearfix">
        <div className="rank-section_text">
        <p>Top</p>
        <div className="rank-section_number clearfix"><div className="rank-section_number_wrapper"><i className="zmdi zmdi-star"></i><span>5</span><i className="zmdi zmdi-star"></i></div></div>
        </div>
        <div className="top_list">
        <ul>
            {liList}
        </ul>
        </div>
    </div>
    )
}
const RenderOwnRank=(props)=>{
    return(
        <div className="user_rank clearfix">
        <p className="user_rank_text">YOUR RANK</p>
        <span className="user_rank_number">{props.data.userData.rank}</span>
       </div>
    )
}
const RenderScore =(props)=>{
    if(props.data && props.data !=='' && props.data !==undefined){
        return (                 
            <div className="single-section">
             <RenderGraph key="1" {...props}/>
      
            <RenderOwnRank key="2" {...props}/>
    
            <RenderTop5 key="3" topFiveUser={props.data.topFiveData}/>
            </div>
        )
    } else {
        return (                 
            <div className="single-section">
             No score has beed configured
            </div>
        )
    }
}

class UserScore extends Component {
  constructor(props){
        super(props);
  }
  componentDidMount(){
    setInterval(this.props.onRefresh,window.refreshGamificationData());
  }
  render(){
      const {allUserScore}=this.props;
      let userScore =[];
      
      if(allUserScore && allUserScore !==''){
          try{
            allUserScore.forEach(data => {
                userScore.push(<RenderScore key={data.scoreId} data={data}/>)
            });
          }catch(e){}
          
      }
      return(
        <div className="col s12 m12 l12 single-section-wrapper">    
         {userScore}
        </div>
     )
   }
}
const mapStateToProps = (state, ownProps) => {
    return {
        allUserScore:(state.userScoreReducer.data === undefined)?'':state.userScoreReducer.data,
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    let userScore=()=>{
        dispatch(getUserScoreByCampaignOrQueue())
    }
   return {
     onRefresh:userScore
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(UserScore)

