import React, { Component } from 'react';
import Highcharts from 'highcharts';
class Chart extends Component {
    componentDidMount() {
        this.chart = new Highcharts[this.props.type || "Chart"](
            this.refs.chart,
            this.props.options
        );
    }

    componentWillUnmount() {
        this.chart.destroy();
    }
    componentWillUpdate() {
       // console.log('Update State',this.props.options);
        this.chart.series[0].setData(this.props.options.series[0].data, true);
        this.chart.setTitle({ text: this.props.options.title.text } , true);
        this.chart.redraw();
   //   this.chart.xAxis[0].setCategories(this.props.options.xAxis.categories, true);
     // this.chart.series[0].setData(this.props.options.series[0].data, true);
      //this.chart.series[1].setData(this.props.options.series[1].data, true);
    }

    render() {
        return (
            <div className= "" ref="chart" style={{height: '150px'}}/>
        )
    }
}
class RenderPieChart extends Component{
    constructor(props){
            super(props);
    }
    render(){



   const optionsDaily = {
    chart: {
        type: 'pie'
       
    },
    title: {
        floating: true,
        text: this.props.totalPercentage+'%',
        y:70,
        style: {
                fontWeight: 'normal',
                fontFamily: 'OpenSans'
        }
    },
    plotOptions: {
        pie: {
                innerSize: 80,
                colors: ['#3db54a','#ecf6fc']
        }
    },   
    exporting: {
        enabled: false
    },
    legend: {
        layout: 'vertical',
        verticalAlign: 'top',
        itemMarginTop: 3,
        itemStyle: {
                fontWeight: 'normal',
                        fontSize: '12px',
                        fontFamily: 'OpenSans'
        },
        enabled: false,
        floating: true,
        align: 'right',
        y: 100,
        x: -10
        },
       
        series: [{
             dataLabels: {
                        enabled: false,
                       
                },
            data:this.props.data
        }]
   };
    return (
             <Chart options={optionsDaily}/>
        );
  }
}
const RenderGraph=(props)=>{
    if(props.data && props.data !== undefined){
    let target= parseInt(props.data.userData.scoreTarget);
    let achived=parseInt(props.data.userData.scoreAchieved);
    let newTarget=0;
    if(achived<target){
      newTarget = target-achived;
    }
    var graphSeriesData=[{name:'Achieved',y:parseInt(props.data.userData.scoreAchieved)},{name:'Target',y:newTarget}];
    
    var totalPercentage=props.data.userData.scoreTarget!==0?(parseInt(props.data.userData.scoreAchieved)/parseInt(props.data.userData.scoreTarget))*100 : 100;
    //console.log('totalPercentage',totalPercentage)
    return(
        <div className="graph-section">
        <div className="graph-section_heading">
        <h3>{props.data.scoreName}</h3>
        </div>
        <div className="graph-section_chart">
        <div id="container" className="main-chart">
        <RenderPieChart data={graphSeriesData} totalPercentage={totalPercentage}/>
        </div>
        </div>
        <div className="graph-section_value clearfix">
        <div className="graph-section_value_achieved left">
        <p>{props.data.userData.scoreAchieved}</p>
        <span>Achieved</span>
        </div>
        <div className="graph-section_value_achieved left">
        <p>{props.data.userData.scoreTarget}</p>
        <span>Target</span>
        </div>
        </div>
       </div>
     )
    } else {
        console.log('i m else',props.data);
        return(
            <div></div>
        )
    }
    
}
export default RenderGraph;